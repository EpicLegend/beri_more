//= ../node_modules/lazysizes/lazysizes.js

//= ../node_modules/jquery/dist/jquery.js

//= ../node_modules/popper.js/dist/umd/popper.js

//= ../node_modules/bootstrap/js/dist/util.js
//= ../node_modules/bootstrap/js/dist/alert.js
//= ../node_modules/bootstrap/js/dist/button.js
//= ../node_modules/bootstrap/js/dist/carousel.js
//= ../node_modules/bootstrap/js/dist/collapse.js
//= ../node_modules/bootstrap/js/dist/dropdown.js
//= ../node_modules/bootstrap/js/dist/modal.js
//= ../node_modules/bootstrap/js/dist/tooltip.js
//= ../node_modules/bootstrap/js/dist/popover.js
//= ../node_modules/bootstrap/js/dist/scrollspy.js
//= ../node_modules/bootstrap/js/dist/tab.js
//= ../node_modules/bootstrap/js/dist/toast.js

//= ../node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js

//= library/slick.js


$(document).ready(function () {
	$(".navbar-toggler").on("click", function() {
		$("header").toggleClass("active");
	});	

	$('.slider_client').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 3,
	  adaptiveHeight: true,
	  slidesToScroll: 3,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	}).on('setPosition', function (event, slick) {
    	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});

	$('.slider_case').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 2,
	  adaptiveHeight: true,
	  slidesToScroll: 2,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 2,
	        slidesToScroll: 2,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	}).on('setPosition', function (event, slick) {
    	slick.$slides.css('height', slick.$slideTrack.height() + 'px');
	});

	$('.carouselPartners').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 5,
	  slidesToScroll: 5,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  responsive: [
	    {
	      breakpoint: 1024,
	      settings: {
	        slidesToShow: 3,
	        slidesToScroll: 3,
	        infinite: true,
	        dots: true
	      }
	    },
	    {
	      breakpoint: 981,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    },
	    {
	      breakpoint: 480,
	      settings: {
	        slidesToShow: 1,
	        slidesToScroll: 1
	      }
	    }
	    // You can unslick at a given breakpoint now by adding:
	    // settings: "unslick"
	    // instead of a settings object
	  ]
	});
	$('.slider_warning_mobile').slick({
	  dots: true,
	  infinite: false,
	  speed: 300,
	  slidesToShow: 1,
	  slidesToScroll: 1,
	  nextArrow: '<div class="sldier_arrow_next"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	  prevArrow: '<div class="sldier_arrow_prev"><svg class="card_item_arrow" xmlns="http://www.w3.org/2000/svg" width="18.098" height="10.316" viewBox="0 0 18.098 10.316"><g id="arrow-up" transform="translate(-1035.793 -4349.188)"><g id="arrow-point-to-right" transform="translate(1053.891 4252.049) rotate(90)" opacity="0.303"><path id="Path_6" data-name="Path 6" d="M107.084,9.945,99.3,17.726a1.267,1.267,0,0,1-1.792-1.792L104.4,9.049,97.51,2.163A1.267,1.267,0,0,1,99.3.371l7.782,7.782a1.267,1.267,0,0,1,0,1.792Z" transform="translate(0 0)"/></g></g></svg></div>',
	});

	if ( $(window).scrollTop() > 0 ) {
        $("header").addClass("bg-dark");
    }
	$(window).on("scroll", function () {
		if ( $(window).scrollTop() > 0 ) {
			$("header").addClass("bg-dark");
		}  else {
			$("header").removeClass("bg-dark");
		}


		if ( $(window).scrollTop() > 100 ) {
			$(".btn_to_top").addClass("active");
		} else {
			$(".btn_to_top").removeClass("active");
		}
	});


	$('.btn_to_top').click(function () {
        $('body,html').animate({
            scrollTop: 0
        }, 400);
        return false;
    });

	
	// select в калькуляторе для мобилки
	$(".select__price").find(".img__arrow").on("click", function() {
		var count = $(this).parent().find(".select__price_wrap");



		count = $(count).find(".select__item").length;

		$(this).parent().find(".select__price_wrap").animate({ height: (25 * count) }, 500);
	});
	var item = $(".select__price_wrap");
	$(item).find(".select__item").on("click", function(){
		var obj = $(this);
		$(this).parent().animate({height: 25  }, 50, function () {
			$(this).prepend( $(obj) );
		});

		calcTabl( $(obj).attr("data-id") );
	});

	function calcTabl(id) {

		var items = $(".calc__hid");
		for (var i = 0 ; i < 2; i++) {
			if ( $(items[i]).attr("data-select") == id ) {
				$(items[i]).removeClass("d-none");
			} else {
				$(items[i]).addClass("d-none");
			}
		}
	}
	$(".calc__form").submit(function (e) {
		e.stopPropagation();
		e.preventDefault();

		var valueCalc = $("input[name=cell]").val();

		var data = [{"0":"","1":"","2":"","3":"0 \u20bd","4":"10%, 960 000 \u20bd","5":"1 \u20bd","6":"2 \u20bd","7":"3 \u20bd","8":"4 \u20bd","total":"5000 \u20bd"},{"0":"","1":"","2":"","3":"0 \u20bd","4":"10%, 960 000 \u20bd","5":"1 \u20bd","6":"2 \u20bd","7":"3 \u20bd","8":"4 \u20bd","total":"6000 \u20bd"}];

		//$.post("http://beri.chernovichok.ru/assets/calc/calc.php", valueCalc, function (e, data) {
		//	console.log(data);
		//});

		var table = $(".calc_tab");
		var length = Object.keys(data[0]).length;
		for (var i = 0; i < 2; i++) {
			var j = 0;
			var item = $( table[i] ).find(".calc__val");
			console.log(item);
			for (var q in data[i]) {
			    if ( data[0][q] == "" ) {
			    	j++;
			    	continue;
			    }
			   	$( item[j] ).html( data[i][q] );
			    j++;
			}
			console.log(i);
		}
	});




});

/*

var items = $(".calc__hid");
		$(items[1]).addClass("d-none");

*/


// адаптив карты
$(window).on("load resize", function (e) {
	if ( $(window).width() > 981 ) {
		$(".calc__hid").removeClass("d-none");
	} else {
		
	}
    positionMapContent(e);
} );

function positionMapContent(e) {
	var padding = $(window).width() - $(".footer .container").width();
	padding = padding / 2;

	$( ".map__absolute" ).css("right", padding);


	console.log(padding);
}